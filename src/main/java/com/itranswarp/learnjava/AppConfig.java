package com.itranswarp.learnjava;

import com.itranswarp.learnjava.service.AppService;
import com.itranswarp.learnjava.service.User;
import com.itranswarp.learnjava.service.UserService;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Properties;

@Configuration
@ComponentScan
@PropertySource("app.properties") // 表示读取classpath的app.properties
@EnableAspectJAutoProxy
public class AppConfig {
    @Value("${app.zone:Z}")  // "${app.zone:Z}"表示从配置文件的app.zone节点取值，未找到就使用默认值Z。
    String zoneId;

    public static void main(String[] args) throws SQLException {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        UserService userService = context.getBean(UserService.class);
        User user = userService.login("meng@qq.com", "123456");
        System.out.println(user.getName() + "logged in!");
        user = userService.register("meng7@qq.com", "123456", "萌7");
        System.out.println(user.getName() + " registered successfully!");
        AppService appService = context.getBean(AppService.class);
        System.out.println(appService.appProperties.get("version"));
    }

    @Bean(name = "dataSource")  //装配第三方Bean
    public HikariDataSource getDataSource() {
        Properties props = new Properties();
        props.setProperty("jdbcUrl", "jdbc:mysql://localhost:3306/test?characterEncoding=utf-8");
        props.setProperty("username", "root");
        props.setProperty("password", "123456");
        HikariConfig config = new HikariConfig(props);
        config.addDataSourceProperty("cachePrepStmts", "true");
        return new HikariDataSource(config);
    }

    @Bean(name = "z")  // 别名
    @Primary  // 指定为主要Bean
    ZoneId createZoneOfZ() {
        return ZoneId.of("Z");
    }

    @Bean
    @Qualifier("utc8")  // 别名
    ZoneId createZoneOfUTC8() {
        return ZoneId.of("UTC+08:00");
    }

    @Bean
    ZoneId createZoneId() {
        return ZoneId.of(this.zoneId);
    }

    @Bean
    @Profile("!test")  // 在运行程序时，加上JVM参数-Dspring.profiles.active=test就可以指定以test环境启动
    ZoneId createDefaultZoneId() {
        return ZoneId.systemDefault();
    }

    @Bean
    @Profile("test")
    ZoneId createZoneIdForTest() {
        return ZoneId.of("America/New_York");
    }
}
