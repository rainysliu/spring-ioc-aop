package com.itranswarp.learnjava.service;

public interface Validator {
    void validate(String email, String password, String name);
}
