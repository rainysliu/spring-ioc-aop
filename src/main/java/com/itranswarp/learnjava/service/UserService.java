package com.itranswarp.learnjava.service;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalLong;

@Component
public class UserService {
    @Autowired
    private MailService mailService;
    @Autowired
    private HikariDataSource dataSource;
    @Autowired
    private Validators validators;

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setValidators(Validators validators) {
        this.validators = validators;
    }

    public void setDataSource(HikariDataSource dataSource) {
        this.dataSource = dataSource;
    }

    private List<User> getUsers() throws SQLException {
        List<User> users = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement ps = conn.prepareStatement("SELECT * FROM users")) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        long id = rs.getLong("id");
                        String email = rs.getString("email");
                        String password = rs.getString("password");
                        String name = rs.getString("name");
                        users.add(new User(id, email, password, name));
                    }
                }
            }
        }
        return users;
    }

    private User addUser(long id, String email, String password, String name) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            try (PreparedStatement ps = conn.prepareStatement(
                    "INSERT INTO users (id, email, password, name) VALUES (?,?,?,?)")) {
                ps.setObject(1, id);
                ps.setObject(2, email);
                ps.setObject(3, password);
                ps.setObject(4, name);
                int n = ps.executeUpdate(); // 1
                if (n == 0) {
                    throw new RuntimeException("insert user failed");
                }
            }
        }
        return new User(id, email, password, name);
    }

    public User login(String email, String password) throws SQLException {
        for (User user : getUsers()) {
            if (user.getEmail().equalsIgnoreCase(email) && user.getPassword().equals(password)) {
                mailService.sendLoginMail(user);
                return user;
            }
        }
        throw new RuntimeException("login failed.");
    }

    public User getUser(long id) throws SQLException {
        return getUsers().stream().filter(user -> user.getId() == id).findFirst().orElseThrow();
    }

    // 监控register()方法性能:
    @MetricTime("register")
    public User register(String email, String password, String name) throws SQLException {
        validators.validate(email, password, name);
        List<User> users = getUsers();
        users.forEach((user) -> {
            if (user.getEmail().equalsIgnoreCase(email)) {
                throw new RuntimeException("email exist.");
            }
        });
        OptionalLong optionalLong = users.stream().mapToLong(User::getId).max();
        long maxId = optionalLong.isPresent() ? optionalLong.getAsLong() : 0;
        User user = addUser(maxId + 1, email, password, name);
        mailService.sendRegistrationMail(user);
        return user;
    }
}
