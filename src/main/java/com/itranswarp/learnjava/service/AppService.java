package com.itranswarp.learnjava.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Component
public class AppService {
    @Value("classpath:/app.properties")
    private Resource resource;

    public Properties appProperties;

    @PostConstruct
    public void init() throws IOException {
        Properties props = new Properties();
        try (var reader = new BufferedReader(
                new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8))) {
            props.load(reader);
        }
        this.appProperties = props;
    }
}
