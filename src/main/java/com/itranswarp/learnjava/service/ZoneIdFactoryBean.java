package com.itranswarp.learnjava.service;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

import java.time.ZoneId;

@Component
// 工厂模式创建Bean
public class ZoneIdFactoryBean implements FactoryBean<ZoneId> {
    String zone = "Z";

    @Override // 覆写getObject方法返回要创建的Bean
    public ZoneId getObject() {
        return ZoneId.of(zone);
    }

    @Override
    public Class<?> getObjectType() {
        return ZoneId.class;
    }
}
