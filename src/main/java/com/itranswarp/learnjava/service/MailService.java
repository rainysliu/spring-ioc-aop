package com.itranswarp.learnjava.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class MailService {
    @Autowired(required = false) // 可选注入
    @Qualifier("utc8")  // 指定别名的Bean
    ZoneId zoneId = ZoneId.systemDefault();

    @Value("#{smtpConfig.host}")  // 从smtpConfig Bean中的getHost注入
    private String smtpHost;

    @Value("#{smtpConfig.port}")  // 从smtpConfig Bean中的getPost注入
    private int smtpPort;
    @PostConstruct  // 初始化方法
    public void init() {
        System.out.println("Init mail service with zoneId = " + this.zoneId);
    }

    @PreDestroy  // 销毁前的清理方法
    public void shutdown() {
        System.out.println("Shutdown mail service");
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public String getTime() {
        return ZonedDateTime.now(this.zoneId).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    public void sendLoginMail(User user) {
        System.err.printf("Hi, %s! You are logged in at %s%n", user.getName(), getTime());
    }

    public void sendRegistrationMail(User user) {
        System.err.printf("Welcome, %s!%n", user.getName());

    }
}
