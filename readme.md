## SpringIoC容器
IoC: Inversion of Control 控制反转

##### 使用xml装配Bean
1. xml中定义DataSource的Bean和其与UserService的依赖关系
2. UserService中直接使用注入的DataSource实例操作SQL

##### 使用注解定义和注入Bean
1. 删除xml配置文件
2. 使用@Component类注解实现Bean类的定义
3. 使用@Autowired字段注解实现Bean自动注入
4. 在Config类中使用@Bean方法注解实现三方Bean的定义

##### 定制Bean
1. 用Scope类注解设置Bean是否是单例模式
2. 注入List，并为各实现类设置顺序
3. 用@Bean("name")或@Bean+@Qualifier("name")指定别名
4. 实现FactoryBean接口创建工厂模式Bean

##### 使用Resource注入资源文件
1. 使用@Value("classpath:/filename")定义资源文件
2. 使用Resource声明资源类型字段;

##### 使用配置
1. 使用@PropertySource引入配置文件
2. 使用@value('$key:default')来读取注入配置文件中的值
3. 使用@value('#key:default')来读取注入JavaBean中的值

##### 条件装配
1. 使用@Profile配置不同条件下的Bean
2. 使用@Condition指定条件装配

## 使用AOP
AOP: Aspect Oriented Programming 面向切面编程

##### 装配AOP
1. 使用@Component和@Aspect标注一个AOP类
2. 在AOP类中使用拦截器定义切面的作用对象
3. 在@Configuration类上标注@EnableAspectJAutoProxy

##### 使用注解装配AOP
1. 定义一个性能监控注解metricTime
2. 在目标方法上userService.register上标注该注解
3. 定义一个AOP类并使用@Around("@annotation(metricTime)")标注AOP方法



